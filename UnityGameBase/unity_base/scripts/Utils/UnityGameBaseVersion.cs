﻿using UnityEngine;
using System.Collections;

namespace UGB.Utils
{
	/// <summary>
	/// Contains the current version of the library
	/// </summary>
	public static class UnityGameBaseVersion
	{
		/// <summary>
		/// Version string
		/// </summary>
		public const string kVersion = "2.0.0";
	}
}